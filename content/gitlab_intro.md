## GitLab

- GitLab is an application to code, test and deploy.
- Provides repository management with access controls, code reviews,
  issue tracking, Merge Requests, and other features.
- The hosted version of GitLab is gitlab.com

----------

## New Project
- Sign in into your gitlab.com account
- Create a project
- Choose to import from 'Any Repo by URL' and use https://gitlab.com/gitlab-org/training-examples.git
- On your machine clone the 'training-examples' project
